package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FileParser {
    FileContent parse(Path filename) throws IOException {
        List<String> lines = Files.readAllLines(filename, Charset.forName("UTF-8"));
        List<String> notEmptyLines = new ArrayList<>();
        for (String member : lines) {
            if (!member.trim().isEmpty()) {
                notEmptyLines.add(member.trim());
            }
        }
        return new FileContent(notEmptyLines);
    }
}
