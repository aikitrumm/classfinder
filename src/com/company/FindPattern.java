package com.company;

import java.util.ArrayList;
import java.util.List;

public class FindPattern {

    ResultFound logic(FileContent fileContent, String pattern) {
        List<String> linesContainingPattern = new ArrayList<>();
        for (String line : fileContent.lines) {
            int lastIndexOfPoint = line.lastIndexOf(".");
            if (findByFullPattern(line.substring(lastIndexOfPoint+1), pattern)) linesContainingPattern.add(line);
            if (findIfAllUpperCaseCharacters(line.substring(lastIndexOfPoint+1), pattern)) linesContainingPattern.add(line);
            if (findIfAllLowerCaseCharacters(line.substring(lastIndexOfPoint+1), pattern)) linesContainingPattern.add(line);

            if (Character.isWhitespace(pattern.charAt(pattern.length() - 1))) {
                if (findIfPatternEndsWithSpace(line.substring(lastIndexOfPoint + 1), pattern)) linesContainingPattern.add(line);
            }
        }

        return new ResultFound(linesContainingPattern);
    }

    boolean findIfPatternEndsWithSpace(String line, String pattern) {
        int lastIndexOfUpperCaseCharacter = lastIndexOfUpperCaseCharacter(pattern);
        String beforeLastWordInPattern = pattern.substring(0, lastIndexOfUpperCaseCharacter);
        String lastWordInPattern = pattern.substring(lastIndexOfUpperCaseCharacter).trim();

        if (findIfAllUpperCaseCharacters(line, beforeLastWordInPattern)) {
            int lastIndex = line.lastIndexOf(lastWordInPattern);
            return line.substring(lastIndex).equals(lastWordInPattern);
        }
        return false;
    }

    boolean findIfAllLowerCaseCharacters(String line, String pattern) {
        if (!pattern.toLowerCase().equals(pattern) ||
                line.length() < pattern.length() ||
                pattern.isEmpty()) return false;
        String upperCasePattern = pattern.toUpperCase();
        return findIfAllUpperCaseCharacters(line, upperCasePattern);
    }


    boolean findIfAllUpperCaseCharacters(String line, String pattern) {
        if (!pattern.toUpperCase().equals(pattern) ||
                line.length() < pattern.length() ||
                pattern.isEmpty()) return false;

        StringBuilder upperCaseCharacters = new StringBuilder();
        for (int i = 0; i < line.length(); i++) {
            char character = line.charAt(i);
            if (Character.isUpperCase(character)) upperCaseCharacters.append(character);
        }

        String checkedLine = upperCaseCharacters.toString();
        for (int i = 0; i < pattern.length(); i++) {
            char characterInPattern = pattern.charAt(i);
            int firstOccuranceInLine = checkedLine.indexOf(characterInPattern);
            if (firstOccuranceInLine == -1) return false;
            checkedLine = checkedLine.substring(firstOccuranceInLine + 1);
        }
        return true;
    }

    boolean findByFullPattern(String line, String pattern) {
        if (line.length() < pattern.length() || pattern.isEmpty()) return false;

        if (pattern.contains("*")) {
            int indexOfStar = pattern.indexOf("*");
            String patternUntilStar = pattern.substring(0, indexOfStar);
            String patternAfterStar = pattern.substring(indexOfStar + 1);
            for (int i = 0; i < line.length() - pattern.length() + 1; i++) {
                boolean equalsBeforeStar = line.substring(i, i + patternUntilStar.length()).equals(patternUntilStar);
                boolean equalsAfterStar = line.substring(i + indexOfStar + 1, i + indexOfStar + 1 + patternAfterStar.length()).equals(patternAfterStar);
                if (equalsBeforeStar && equalsAfterStar) {
                    return true;
                }
            }
        } else {
            for (int i = 0; i < line.length() - pattern.length() + 1; i++) {
                if (line.substring(i, i + pattern.length()).equals(pattern)) {
                    return true;
                }
            }
        }
        return false;
    }

    int lastIndexOfUpperCaseCharacter(String pattern) {
        for (int i = pattern.length() - 1; i >= 0; i--) {
            if (Character.isUpperCase(pattern.charAt(i))) {
                return i;
            }
        }
        return -1;
    }
}
