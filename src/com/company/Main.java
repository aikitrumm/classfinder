package com.company;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException {

        Path filename = Paths.get(args[0]);
        String pattern = args[1];
//        String pattern = "FBar ";
//        Path filename = Paths.get("classes.txt");
        FileParser fileParser = new FileParser();
        FileContent fileContent = fileParser.parse(filename);

        FindPattern findPattern = new FindPattern();
        ResultFound resultFound = findPattern.logic(fileContent, pattern);

        Sort sortedList = new Sort(resultFound);

        for (String member: sortedList.sortedList) {
            System.out.println(member);
        }
    }

}
