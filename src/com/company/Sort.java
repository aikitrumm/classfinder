package com.company;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sort {
    List<String> sortedList;

    public Sort(ResultFound list) {
        this.sortedList = list.linesContainingPattern;
        this.sortedList.sort(Collator.getInstance());

        Collections.sort(this.sortedList, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                int lastIndexOfPointS1 = s1.lastIndexOf(".");
                int lastIndexOfPointS2 = s2.lastIndexOf(".");
                return s1.substring(lastIndexOfPointS1 + 1).compareToIgnoreCase(s2.substring(lastIndexOfPointS2 + 1));
            }
        });
    }


}
