package com.company;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class FileParserTest {

    private FileParser fileParser = new FileParser();

    @Test
    public void parse() throws IOException {
        FileContent fileContent = fileParser.parse(Paths.get("classes.txt"));

        assertEquals("a.b.FooBarBaz", fileContent.lines.get(0));
        assertEquals("c.d.FooBar", fileContent.lines.get(1));
        assertEquals("codeborne.WishMaker", fileContent.lines.get(2));
        assertEquals("codeborne.MindReader", fileContent.lines.get(3));
        assertEquals("TelephoneOperator", fileContent.lines.get(4));
        assertEquals("ScubaArgentineOperator", fileContent.lines.get(5));
        assertEquals("YoureLeavingUsHere", fileContent.lines.get(6));
        assertEquals("YouveComeToThisPoint", fileContent.lines.get(7));
        assertEquals("YourEyesAreSpinningInTheirSockets", fileContent.lines.get(8));
        assertEquals(9, fileContent.lines.size());
    }
}