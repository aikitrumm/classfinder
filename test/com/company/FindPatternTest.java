package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class FindPatternTest {

    private FindPattern findPattern = new FindPattern();

    @Test
    public void findIfAllUpperCaseCharacters() {
        assertTrue(findPattern.findIfAllUpperCaseCharacters("YourEyesAreSpinningInTheirSockets", "YS"));
        assertTrue(findPattern.findIfAllUpperCaseCharacters("YourEyesAreSpinningInTheirSockets", "YEASITS"));
        assertFalse(findPattern.findIfAllUpperCaseCharacters("YourEyesAreSpinningInTheirSockets", ""));
        assertFalse(findPattern.findIfAllUpperCaseCharacters("YourEyesAreSpinningInTheirSockets", "Ts"));
        assertTrue(findPattern.findIfAllUpperCaseCharacters("a.b.FooBarBaz", "FB"));
        assertFalse(findPattern.findIfAllUpperCaseCharacters("a.b.FooBarBaz", "BF"));

    }

    @Test
    public void findByFullPattern() {
        assertTrue(findPattern.findByFullPattern("a.b.FooBarBaz", "FooBar"));
        assertFalse(findPattern.findByFullPattern("a.b.FooBarBaz", "Foobar"));
        assertFalse(findPattern.findByFullPattern("a.b.FooBarBaz", "FooBaz"));
        assertFalse(findPattern.findByFullPattern("a.b.FooBarBaz", ""));
    }

    @Test
    public void findIfAllLowerCaseCharacters() {
        assertTrue(findPattern.findIfAllLowerCaseCharacters("YourEyesAreSpinningInTheirSockets", "ys"));
        assertTrue(findPattern.findIfAllLowerCaseCharacters("YourEyesAreSpinningInTheirSockets", "yeasits"));
        assertFalse(findPattern.findIfAllLowerCaseCharacters("YourEyesAreSpinningInTheirSockets", "yeaSITs"));
        assertFalse(findPattern.findIfAllLowerCaseCharacters("YourEyesAreSpinningInTheirSockets", ""));
        assertFalse(findPattern.findIfAllLowerCaseCharacters("YourEyesAreSpinningInTheirSockets", "Ts"));
    }

    @Test
    public void lastIndexOfUpperCaseCharacter() {
        assertEquals(1, findPattern.lastIndexOfUpperCaseCharacter("FBar"));
        assertEquals(2, findPattern.lastIndexOfUpperCaseCharacter("FFBar"));
    }

    @Test
    public void findIfPatternEndsWithSpace() {
        assertTrue(findPattern.findIfPatternEndsWithSpace("a.b.FooBar", "FBar "));
        assertFalse(findPattern.findIfPatternEndsWithSpace("a.b.FooBarBaz", "FBar "));
    }
}